# InfiniteScrollerRxjs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#Infinite-scroller
we will create an Angular 6 directive to build the infinite scroll feature. We will use the HackerNews Unofficial API to populate our container.

#RXJS OPERATORS
1. map : similar to map in an array, map over the stream of incoming data.

2. filter : similar to filter in an array, filter the stream of incoming data.

3. pairwise : returns an array of current emitted data and also the previous emitted data .

4. startWith : returns an observable emits supplied values before emitting the values from source observable

5. exhaustMap : waits to emit value until the passed in inner observable has completed